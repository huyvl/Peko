<header>
    <section class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-4">
                    <div class="logo">
                        <a class="img-responsive" href="{{url('/')}}" title=""><img src="{{ asset('images/logo/logo.png') }}"></a>
                    </div>
                </div>
                <div class="col-md-10 col-8">
                    <div class="header-right">
                        <div class="flex-center-end">
                            <ul class="flex-center langue sub-option">
                                <li><a href="{{url('lang/en')}}" title="">@lang('label.english')</a> </li>
                                <li><a href="{{url('lang/vn')}}" title="">@lang('label.vietnamese')</a> </li>
                            </ul>
                        </div>
                        <div class="visible-mobile nav-mobile text-right relative">
                            <a href="#0" title="" class="nav-account"><i class="fa fa-user-circle-o"></i></a>
                            <a id="cd-menu-trigger" href="#0" class=""><span class="cd-menu-icon"></span></a>
                        </div>
                        <div class="account-nav">
                            <ul>
                                <li>
                                    <a href="" title="">
                                        @lang('label.header.Login')
                                    </a>
                                </li>
                                <li>
                                    <a href="" title="">
                                        @lang('label.header.Signup')
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="header-nav">
                        <div class="nav-top">
                            <ul class="flex-center-start">
                                <li><a href="{{url('/')}}" title="">@lang('label.header.Service')</a></li>
                                <li><a href="{{url('work')}}" title="">@lang('label.header.Work')</a></li>
                                <li><a href="{{url('shop')}}" title="">@lang('label.header.Shop')</a></li>
                                <li><a href="{{url('partner')}}" title="">@lang('label.header.Partner')</a></li>
                                <li><a href="{{url('guide')}}" title="">@lang('label.header.Guide')</a></li>
                            </ul>
                        </div>
                        <div class="login flex-center">
                            <div class="login-image">
                                <a href="" title=""><img src="{{ asset('images/icon/icon-login.png') }}" alt="" title=""> </a>
                            </div>
                            <div class="login-link">
                                {{--<a href="{{url('/')}}" title="">@lang('label.header.Login')</a>--}}
                                <p>@lang('label.header.Member') <a href="{{url('/signup')}}" title="">@lang('label.header.Signup')</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</header>
