<nav id="cd-lateral-nav" class=" visible-mobile">
    <ul class="cd-navigation nav-dropdown">
        <li><a href="{{url('/')}}" title="">@lang('label.header.Service')</a> </li>
        <li><a href="{{url('work')}}" title="">@lang('label.header.Work')</a> </li>
        <li><a href="{{url('shop')}}" title="">@lang('label.header.Shop')</a> </li>
        <li><a href="{{url('partner')}}" title="">@lang('label.header.Partner')</a> </li>
        <li><a href="{{url('guide')}}" title="">@lang('label.header.Guide')</a> </li>
    </ul>
</nav>