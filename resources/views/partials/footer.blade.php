<footer>
    <section class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="logo-footer">
                        <a href="{{url('/')}}" title=""><img src="{{ asset('images/logo/logo.png') }}" alt="" title=""> </a>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="footer-info">
                        <h2 class="text-uppercase">@lang('label.footer.Company')</h2>
                        <p><i class="fa fa-phone"></i> <a href="" title=""> +84 43915 8888</a></p>
                        <p><i class="fa fa-envelope-o"></i> <a href="" title="">HELLO@PEKOPEKO.VN </a></p>
                        <p><i class="fa fa-map-marker"></i> @lang('label.footer.Address')</p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="social">
                        <a href="{{url('https://www.facebook.com/pekoapp/')}}" title="" class="fa fa-facebook inflex-center-center"></a>
                        <a href="{{url('/')}}" title="" class="fa fa-instagram inflex-center-center"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>