@extends('layouts.index')
@section('content')
    <div class="cd-main-content">
        <section class="choose-us">
            <div class="container">
                <h1 class="pages-choose-title title-page text-center">@lang('label.Why-choose.Whychoose')</h1>
                <div class="pages-choose--link text-center">
                    <a class="_link shop inflex-center-center active" id="click-shop">
                        <div class="link-in">
                            <span>
                            <img class="links__icon img-nor" src="{{ asset('images/banner/shop-1-t.png') }}" alt=""
                                 title="">
                            <img class="links__icon img-hover" src="{{ asset('images/banner/shop-1.png') }}" alt=""
                                 title="">
                        </span>
                            <span class="link_detail">@lang('label.Why-choose.Shop')</span>
                        </div>
                    </a>
                    <a class="_link customer inflex-center-center" id="click-customer">
                        <div class="link-in">
                            <span>
                                <img class="links__icon img-nor" src="{{ asset('images/banner/shop-2.png') }}" alt=""
                                     title="">
                                <img class="links__icon img-hover" src="{{ asset('images/banner/shop-2-w.png') }}"
                                     alt="" title="">
                            </span>
                            <span class="link_detail">@lang('label.Why-choose.Customer')</span>
                        </div>
                    </a>
                </div>
                <div id="forshop" class="">
                    <h2 class="text-center">@lang('label.Why-choose.Shop') </h2>
                    <p class="guide-subtitle text-center">@lang('label.Why-choose.be')</p>
                </div>
                <div id="forcustomer" class="hide">
                    <h2 class="text-center">@lang('label.Why-choose.Customer') </h2>
                    <p class="guide-subtitle text-center">@lang('label.Why-choose.beY')</p>
                </div>
            </div>
        </section>
        <section class="reach-customer" id="toshop">
            <div class="container">
                <div class="reach-customer__content">
                    <div class="reach-customer--item">
                        <div class="card-deck mb-3 text-center">
                            <div class="card mb-4 box-shadow" style="background: none">
                                <div class="card-header" style="background: #e6ba06">
                                    <h4 class="my-0 font-weight-normal">@lang('label.Why-choose.Reach')</h4>
                                </div>
                                <div class="card-body">
                                    <br>
                                    <img src="{{ asset('images/banner/how-work-2.png') }}" alt="" title="">
                                    <ul class="list-unstyled mt-3 mb-4">
                                        <li>@lang('label.Why-choose.Setup')</li>
                                        <li>@lang('label.Why-choose.customise')</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card mb-4 box-shadow" style="background: none">
                                <div class="card-header" style="background: #e6ba06">
                                    <h4 class="my-0 font-weight-normal">@lang('label.Why-choose.shop1')</h4>
                                    <br>
                                </div>
                                <div class="card-body">
                                    <br>
                                    <img src="{{ asset('images/banner/how-work-2.png') }}" alt="" title="">
                                    <ul class="list-unstyled mt-3 mb-4">
                                        <li>@lang('label.Why-choose.shop2')</li>
                                        <li>@lang('label.Why-choose.shop3')</li>
                                        <li>@lang('label.Why-choose.shop4')</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card mb-4 box-shadow" style="background: none">
                                <div class="card-header" style="background: #e6ba06">
                                    <h4 class="my-0 font-weight-normal">@lang('label.Why-choose.Reach')</h4>
                                </div>
                                <div class="card-body">
                                    <br>
                                    <img src="{{ asset('images/banner/how-work-2.png') }}" alt="" title="">
                                    <ul class="list-unstyled mt-3 mb-4">
                                        <li>@lang('label.Why-choose.attractive')</li>
                                        <li>@lang('label.Why-choose.attractive1')</li>
                                        <li>@lang('label.Why-choose.attractive2')</li>
                                        <li>@lang('label.Why-choose.attractive3')</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="actions">
                        <a href="{{url('shop')}}" title="" class="btn-signup">@lang('label.Why-choose.Signup') </a>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <section class="reach-customer hide" id="tocustomer">
            <div class="container">
                <div class="reach-customer__content">
                    <div class="reach-customer--item">
                        <div class="card-deck mb-3 text-center">
                            <div class="card mb-3 box-shadow" style="background: none">
                                <div class="card-header" style="background: #e6ba06">
                                    <h4 class="my-0 font-weight-normal">@lang('label.Why-choose.Setting1')</h4>
                                </div>
                                <div class="card-body">
                                    <br>
                                    <img src="{{ asset('images/banner/how-work-2-t.png') }}" alt="" title="">
                                    <ul class="list-unstyled mt-3 mb-4">
                                        <li>@lang('label.Why-choose.Setting2')</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card mb-3 box-shadow" style="background: none">
                                <div class="card-header" style="background: #e6ba06">
                                    <h4 class="my-0 font-weight-normal">@lang('label.Why-choose.Setting3')</h4>
                                </div>
                                <div class="card-body">
                                    <br>
                                    <img src="{{ asset('images/banner/how-work-2-t.png') }}" alt="" title="">
                                    <ul class="list-unstyled mt-3 mb-4">
                                        <li>@lang('label.Why-choose.Setting4')</li>
                                        <li>@lang('label.Why-choose.Setting5')</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card mb-3 box-shadow" style="background: none">
                                <div class="card-header" style="background: #e6ba06">
                                    <h4 class="my-0 font-weight-normal">@lang('label.Why-choose.Refer0')</h4>
                                </div>
                                <div class="card-body">
                                    <br>
                                    <img src="{{ asset('images/banner/how-work-2-t.png') }}" alt="" title="">
                                    <ul class="list-unstyled mt-3 mb-4">
                                        <li>@lang('label.Why-choose.Refer1')</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card mb-3 box-shadow" style="background: none">
                                <div class="card-header" style="background: #e6ba06">
                                    <h4 class="my-0 font-weight-normal">@lang('label.Why-choose.Refer2')</h4>
                                </div>
                                <div class="card-body">
                                    <br>
                                    <img src="{{ asset('images/banner/how-work-2-t.png') }}" alt="" title="">
                                    <ul class="list-unstyled mt-3 mb-4">
                                        <li>@lang('label.Why-choose.Refer3')</li>
                                        <li>@lang('label.Why-choose.Refer4')</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="actions">
                        <a href="{{url('signup')}}" title="" class="btn-signup">@lang('label.Why-choose.Signup') </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
    </div>
@endsection