@extends('layouts.index')
@section('content')
    <div class="cd-main-content">
        <section class="login-container relative">
            <div class="banner-slider owl-carousel">
                <img src="{{ asset('images/banner/banner.jpg') }}" alt="" title="">
                <img src="{{ asset('images/banner/banner-2.jpg') }}" alt="" title="">
            </div>
            <div class="login-form">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="flex-end-center height-100">
                                <div class="wallet">
                                    <h1 class="text-uppercase"><span>Peko</span><br/>@lang('label.For-shop.ShopX')</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <form id="form_signup">
                                {{--{{ csrf_field() }}--}}
                                <div class="form-sign-up">
                                    <div class="form-gr">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p>@lang('label.For-shop.City')</p>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="city" id="txtcity" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-gr">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p>@lang('label.For-shop.First')</p>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="firstName" id="txtfirstName" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-gr">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p>@lang('label.For-shop.Last')</p>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="lastName" id="txtlastName" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-gr">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p>@lang('label.For-shop.Phone')</p>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="phoneNumber" id="txtphoneNumber" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-gr">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p>@lang('label.For-shop.Shop')</p>
                                                <p class="font-italic">@lang('label.For-shop.optional')</p>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="shopName" id="txtshopName" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-gr">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p>@lang('label.For-shop.Shopaddress')</p>
                                                <p class="font-italic">@lang('label.For-shop.optional')</p>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="shopAddress" id="txtshopAddress" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-gr">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p>@lang('label.For-shop.Number')</p>
                                                <p class="font-italic">@lang('label.For-shop.optional')</p>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="number" name="numberOfOutlets" id="txtnumberOfOutlets"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wallet">
                                        <button type="submit" class="btn-signupX"
                                                id="btn_Signup">@lang('label.header.Signup')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="why-join">
            <div class="container">
                <h2 class="text-center title-page">@lang('label.For-shop.Whyjoin')</h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="why-item text-center">
                            <div class="height-gen">
                                <img src="{{ asset('images/banner/why-1.png') }}" alt="" title="">
                            </div>
                            <p>@lang('label.For-shop.Full')</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="why-item text-center">
                            <div class="height-gen">
                                <img src="{{ asset('images/banner/why-4.png') }}" alt="" title="">
                            </div>
                            <p>@lang('label.For-shop.Increaseinreach')</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="why-item text-center">
                            <div class="height-gen">
                                <img src="{{ asset('images/banner/why-6.png') }}" alt="" title="">
                            </div>
                            <p>@lang('label.For-shop.Reduce')</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="how-work">
            <div class="container">
                <h2 class="text-center title-page">@lang('label.For-shop.Howpekoworks')</h2>
            </div>
            <div class="how-inner">
                <div class="container">
                    <div class="pd-200">
                        <div class="how-slider owl-carousel slider-general">
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img src="{{ asset('images/banner/how-work.png') }}" alt="" title="">
                                    </div>
                                    <div class="col-md-7 flex-center">
                                        <div class="work-text">
                                            <h6>@lang('label.For-work.Step') <span class="inflex-center-center">1</span>
                                            </h6>
                                            <p>@lang('label.For-shop.Step1')<br/>
                                                @lang('label.For-shop.Step2')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img src="{{ asset('images/banner/how-work.png') }}" alt="" title="">
                                    </div>
                                    <div class="col-md-7 flex-center">
                                        <div class="work-text">
                                            <h6>@lang('label.For-work.Step') <span class="inflex-center-center">2</span>
                                            </h6>
                                            <p>@lang('label.For-shop.Step3')<br/>
                                                @lang('label.For-shop.Step4')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img src="{{ asset('images/banner/how-work.png') }}" alt="" title="">
                                    </div>
                                    <div class="col-md-7 flex-center">
                                        <div class="work-text">
                                            <h6>@lang('label.For-work.Step') <span class="inflex-center-center">3</span>
                                            </h6>
                                            <p>@lang('label.For-shop.Step5')<br/>
                                                @lang('label.For-shop.Step6')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="join-peko">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <a href="{{url('https://itunes.apple.com/vn/app/peko-rewards-wallet/id1146328400?l=vi&mt=8')}}"
                           title="" class="inflex-center-center">@lang('label.For-shop.Joinpeko')</a>
                        <p class="text-center">@lang('label.For-shop.Whywait')</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection