@extends('layouts.index')
@section('content')
    <div class="cd-main-content">
        <section class="peko-d">
            <div class="container">
                <div class="for-work--title">
                    <h2 class="_title for-work__title text-uppercase">@lang('label.Partner.Build')
                        <br/>@lang('label.Partner.ecosystem')</h2>
                    <a href="#id2" class="btn btn-submit btn-large">@lang('label.Partner.apply')</a>
                </div>
            </div>
        </section>
        <section class="banner-for-work">
            <img src="{{ asset('images/banner/banner-5.jpg') }}" alt="" title="">
        </section>
        <section class="why-join">
            <div class="container">
                <p class="text-center mgb-20"><img src="{{ asset('images/icon/icon-login-lg.png') }}" alt="" title=""></p>
                <h2 class="text-center title-page">@lang('label.Partner.Peko')</h2>
            </div>
        </section>
        <section class="bg-t">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="why-item text-center">
                            <p class="txt-v text-uppercase">@lang('label.Partner.App')</p>
                            <p class="text-uppercase mgb-20">@lang('label.Partner.Partners')</p>
                            <div class="height-gen">
                                <img src="{{ asset('images/banner/part-1.png') }}" alt="" title="">
                            </div>
                            <p>@lang('label.Partner.Have')</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="why-item text-center">
                            <p class="txt-v">@lang('label.Partner.Merchant')</p>
                            <p class="text-uppercase mgb-20">@lang('label.Partner.Partners')</p>
                            <div class="height-gen">
                                <img src="{{ asset('images/banner/shop-1.png') }}" alt="" title="">
                            </div>
                            <p>@lang('label.Partner.Own')</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="why-item text-center">
                            <p class="txt-v text-uppercase">@lang('label.Partner.Cross')</p>
                            <p class="text-uppercase mgb-20">@lang('label.Partner.Partners')</p>
                            <div class="height-gen">
                                <img src="{{ asset('images/banner/part-3.png') }}" alt="" title="">
                            </div>
                            <p>@lang('label.Partner.Want')</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="apply-peko" id="id2">
            <div class="container">
                <h2 class="title-page text-center">@lang('label.Partner.Apply')</h2>
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <form id="form-signupP" class="apply-form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="@lang('label.For-shop.Fullname')" name="fullName" id="txtfullName" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="email" placeholder="@lang('label.For-work.Email')" name="email" id="txtemail" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="@lang('label.For-shop.Phone')" name="phone" id="txtphone" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="@lang('label.For-work.Company')" name="company" id="txtcompany" required>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea placeholder="Mô tả đối tượng khách hàng của bạn" name="description" id="txtdescription" required></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea placeholder="Mô tả ý tưởng hợp tác" name="idea" id="txtidea" required></textarea>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-submit btn-large" id="btn_SignupP">@lang('label.Partner.Submit')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection