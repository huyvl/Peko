@extends('layouts.index')
@section('content')
    <div class="cd-main-content">
        <section class="peko-d">
            <div class="container">
                <div class="for-work--title">
                    <h2 class="_title for-work__title">@lang('label.header.Work')</h2>
                    <a class="btn btn-submit btn-large" href="#id1">@lang('label.For-work.Sign')</a>
                </div>
            </div>
        </section>
        <section class="banner-for-work">
            <img src="{{ asset('images/banner/banner-5.jpg') }}" alt="" title="">
            <div class="content-bn">
                @lang('label.Corporate')
            </div>
        </section>
        <section class="why-join">
            <div class="container">
                <h2 class="text-center title-page">@lang('label.For-work.WhyFor')</h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="why-item text-center">
                            <div class="height-gen">
                                <img src="{{ asset('images/banner/why-1.png') }}" alt="" title="">
                            </div>
                            <h6 class="text-uppercase">@lang('label.Increase')</h6>
                            <p>@lang('label.Increase2')</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="why-item text-center">
                            <div class="height-gen">
                                <img src="{{ asset('images/banner/why-2.png') }}" alt="" title="">
                            </div>
                            <h6 class="text-uppercase">@lang('label.Multiple')</h6>
                            <p>@lang('label.Multiple2')</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="why-item text-center">
                            <div class="height-gen">
                                <img src="{{ asset('images/banner/why-3.png') }}" alt="" title="">
                            </div>
                            <h6 class="text-uppercase">@lang('label.Better')</h6>
                            <p>@lang('label.Better2')</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="how-work">
            <div class="container">
                <h2 class="text-center title-page">@lang('label.For-work.Howto')</h2>
            </div>
            <div class="how-inner">
                <div class="container">
                    <div class="pd-200">
                        <div class="how-slider owl-carousel slider-general">
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img src="{{ asset('images/banner/how-work-3.png') }}" alt="" title="">
                                    </div>
                                    <div class="col-md-7 flex-center">
                                        <div class="work-text">
                                            <h6>@lang('label.For-work.Step') <span class="inflex-center-center">1</span></h6>
                                            <p>@lang('label.For-work.Signup')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img src="{{ asset('images/banner/how-work-3.png') }}" alt="" title="">
                                    </div>
                                    <div class="col-md-7 flex-center">
                                        <div class="work-text">
                                            <h6>@lang('label.For-work.Step') <span class="inflex-center-center">2</span></h6>
                                            <p>@lang('label.For-work.Signup2')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img src="{{ asset('images/banner/how-work-3.png') }}" alt="" title="">
                                    </div>
                                    <div class="col-md-7 flex-center">
                                        <div class="work-text">
                                            <h6>@lang('label.For-work.Step') <span class="inflex-center-center">3</span></h6>
                                            <p>@lang('label.For-work.Signup3')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="form-for-work" id="id1">
            <div class="container">
                <h2 class="title-page">@lang('label.For-work.Sign')</h2>
                <div class="row">
                    <div class="col-md-6">
                        <form id="form-signupW">
                            <fieldset class="fieldset-signup">
                                <div class="field firstName">
                                    <label>@lang('label.For-shop.First')</label>
                                    <input type="text" name="firstName" id="txtfirstName" required>
                                </div>
                                <div class="field lastName">
                                    <label>@lang('label.For-shop.Last')</label>
                                    <input type="text" name="lastName" id="txtlastName" required>
                                </div>
                                <div class="field email">
                                    <label>@lang('label.For-work.Email')</label>
                                    <input type="email" name="email" id="txtemail" required>
                                </div>
                                <div class="field phone">
                                    <label>@lang('label.For-work.Mobile')</label>
                                    <input type="text" name="phone" id="txtphone" required>
                                </div>
                                <div class="field company">
                                    <label>@lang('label.For-work.Company')</label>
                                    <input type="text" name="company" id="txtcompany" required>
                                </div>
                                <div class="field jobTitle">
                                    <label>@lang('label.For-work.Jobtitle')</label>
                                    <select name="jobTitle" id="txtjobTitle" required>
                                        <option value = "1">--@lang('label.For-work.Please')--</option>
                                        <option value = "2">Chief Executive Officer / Founder</option>
                                        <option value = "3">Business Manager</option>
                                        <option value = "4">Account Executive</option>
                                        <option value = "5">Operations Manager</option>
                                        <option value = "6">Administrative Assistant</option>
                                        <option value = "7">Office Manager</option>
                                        <option value = "8">Other</option>
                                    </select>
                                </div>

                                <div class="field companySize">
                                    <label>@lang('label.For-work.Companysize')</label>
                                    <select name="companySize" id="txtcompanySize" required>
                                        <option value = "1">--@lang('label.For-work.Please')--</option>
                                        <option value = "2">Under 100 staffs</option>
                                        <option value = "3">100 to 500 staffs</option>
                                        <option value = "4">more than 500 staffs</option>
                                    </select>
                                </div>
                                <div class="field transportExpenses">
                                    <label>@lang('label.For-work.Monthly')</label>
                                    <select name="transportExpenses" id="txttransportExpenses" required>
                                        <option value = "1">--@lang('label.For-work.Please')--</option>
                                        <option value = "2">Under 1000$ / month</option>
                                        <option value = "3">1000 to 5000$ / month</option>
                                        <option value = "4">more than 5000$ / month</option>
                                    </select>
                                </div>
                                <div class="actions-toolbar">
                                    <button type="submit" class="btn btn-submit" class="btn-signupW"
                                                id="btn_SignupW">@lang('label.For-work.Submit')</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="form-work-text">
                            <p>@lang('label.For-work.Downloadpeko') </p>
                            <a href="{{url('https://play.google.com/store/apps/details?id=peko.honey&hl=vi')}}" title=""><img src="{{ asset('images/icon/google-play.png') }}" alt="" title=""></a>
                            <a href="{{url('https://itunes.apple.com/vn/app/peko-rewards-wallet/id1146328400?l=vi&mt=8')}}" title=""><img src="{{ asset('images/icon/app-store.png') }}" alt="" title=""></a>
                            <p><img src="{{ asset('images/banner/banner-3.png') }}" alt="" title=""></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection