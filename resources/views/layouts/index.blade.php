<!DOCTYPE html>
<html lang="en">
@include('partials.metadata')
<body>
@include('partials.header')

@yield('content')

@include('partials.footer')

<!--Menu on mobile-->

@include('partials.nav')

<!--Link js-->
@include('partials.js_lib')
</body>
</html>