@extends('layouts.index')
@section('content')
    <div class="cd-main-content">
        <section class="login-container relative">
            <div class="banner-slider owl-carousel">
                <img src="{{ asset('images/banner/banner.jpg') }}" alt="" title="">
                <img src="{{ asset('images/banner/banner-2.jpg') }}" alt="" title="">
            </div>
            <div class="login-form" style="top:300px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="flex-end-center height-100">
                                <div class="wallet">
                                    <h1 class="text-uppercase"><span>Peko</span><br/>@lang('label.RewardWallet')</h1>
                                    <a href="{{url('https://itunes.apple.com/vn/app/peko-rewards-wallet/id1146328400?l=vi&mt=8')}}"
                                       title="" class="down-link">@lang('label.Download')</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            {{--<div class="block-customer-login" id="result">--}}
                                {{--<img class="logo-form" src="{{ asset('images/logo/logo.png') }}" alt="" title="">--}}
                                {{--<h1 class="pages-title"><span>@lang('label.header.Signup')</span></h1>--}}
                                {{--<form class="form-login" action="" id="loginCampaign">--}}
                                    {{--{{ csrf_field() }}--}}
                                    {{--<fieldset class="fieldset-login">--}}
                                        {{--<div class="field phone required">--}}
                                            {{--<div class="controls relative">--}}
                                                {{--<input type="text" name="phone" placeholder="@lang('label.SignUp.Phone')">--}}
                                                {{--<div class="absolute flex-center country">--}}
                                                    {{--<div id="show-country" class="show-country">--}}
                                                        {{--<span class="sing"></span>--}}
                                                    {{--</div>--}}
                                                    {{--<select class="choice-country">--}}
                                                        {{--<option value="sing">+65</option>--}}
                                                        {{--<option value="vn">+84</option>--}}
                                                    {{--</select>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="field password required">--}}
                                            {{--<label>@lang('label.For-shop.Password')</label>--}}
                                            {{--<input type="password" name="password">--}}
                                        {{--</div>--}}
                                        {{--<div class="captcha">--}}
                                            {{--<img src="{{ asset('images/banner/captcha.png') }}">--}}
                                        {{--</div>--}}
                                        {{--<div class="pslogin-login">--}}
                                            {{--<a class="login-facebook ps-social" href="">--}}
                                                {{--<span class="pslogin-icon"><i class="fa fa-facebook"--}}
                                                                              {{--aria-hidden="true"></i></span>--}}
                                                {{--<span class="pslogin-detail">@lang('label.SignUp.Facebook')</span>--}}
                                            {{--</a>--}}
                                            {{--<a class="login-google ps-social" href="">--}}
                                                {{--<span class="pslogin-icon"><i class="fa fa-google-plus"--}}
                                                                              {{--aria-hidden="true"></i></span>--}}
                                                {{--<span class="pslogin-detail">@lang('label.SignUp.Google')</span>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="actions-toolbar text-center">--}}
                                            {{--<button type="submit" class="btn submit" id="btnlogin">@lang('label.header.Login')</button>--}}
                                            {{--<p><a href="" title="">@lang('label.SignUp.Forgot')</a></p>--}}
                                        {{--</div>--}}
                                    {{--</fieldset>--}}
                                {{--</form>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="how-peko">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="how-peko-text">
                            <h3>@lang('label.How.How')</h3>
                            <div class="flex-center how-step">
                                <span class="inflex-center-center bg-y">1</span>
                                <div class="how-text">
                                    <p>@lang('label.How.Peko')</p>
                                </div>
                            </div>
                            <div class="flex-center how-step">
                                <span class="inflex-center-center">2</span>
                                <div class="how-text">
                                    <p>@lang('label.How.Rewards')</p>
                                </div>
                            </div>
                            <div class="flex-center how-step">
                                <span class="inflex-center-center bg-y">3</span>
                                <div class="how-text">
                                    <p>@lang('label.How.voucher')</p>
                                </div>
                            </div>
                            <p>@lang('label.How.Pay') <span class="txt-lg">2</span> @lang('label.How.Easy')</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="how-peko-image">
                            <img src="images/banner/banner-3.png" alt="" title="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="why-choose">
            <div class="container">
                <div class="flex-center-center">
                    <span>@lang('label.Why')</span>
                    <a href="{{url('guide')}}" title="">@lang('label.Learn')</a>
                </div>
            </div>
        </section>
        <section class="how-peko-serves relative">
            <img src="{{ asset('images/banner/banner-4.jpg') }}" alt="" title="">
            <div class="how-abs">
                <div class="container">
                    <h2>@lang('label.For-work.Please')</h2>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="ser">
                                <img src="{{ asset('images/banner/ser-1.png') }}" alt="" title="">
                                <h6 class="text-uppercase">Peko Pay </h6>
                                <p></p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="ser">
                                <img src="{{ asset('images/banner/ser-2.png') }}" alt="" title="">
                                <h6 class="text-uppercase">Peko Coin Market </h6>
                                <p></p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="ser">
                                <img src="{{ asset('images/banner/ser-3.png') }}" alt="" title="">
                                <h6 class="text-uppercase">Peko Shops </h6>
                                <p></p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="ser">
                                <img src="{{ asset('images/banner/ser-4.png') }}" alt="" title="">
                                <h6 class="text-uppercase">Peko Delivery</h6>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="dowload-peko">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-right">
                        <img src="{{ asset('images/icon/icon-login-lg.png') }}" alt="" title="">
                        <span>@lang('label.App')</span>
                    </div>
                    <div class="col-md-6">
                        <a href="{{url('https://play.google.com/store/apps/details?id=peko.honey&hl=vi')}}" title=""><img src="{{ asset('images/icon/google-play.png') }}" alt="" title=""></a>
                        <a href="{{url('https://itunes.apple.com/vn/app/peko-rewards-wallet/id1146328400?l=vi&mt=8')}}" title=""><img src="{{ asset('images/icon/app-store.png') }}" alt="" title=""></a>
                    </div>
                </div>
            </div>
        </section>
        {{--<section class="last-news">--}}
            {{--<div class="container">--}}
                {{--<h2 class="text-center text-uppercase">@lang('label.News')</h2>--}}
                {{--<div class="news-slider slider-general owl-carousel">--}}
                    {{--<div class="item">--}}
                        {{--<div class="news-item">--}}
                            {{--<a href="" title=""><img src="{{ asset('images/news/news-1.jpg') }}" alt="" title=""> </a>--}}
                            {{--<p class="text-center"><a href="" title="">ROYAL TEA PROMOTION</a></p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="item">--}}
                        {{--<div class="news-item">--}}
                            {{--<a href="" title=""><img src="{{ asset('images/news/news-1.jpg') }}" alt="" title=""> </a>--}}
                            {{--<p class="text-center"><a href="" title="">ROYAL TEA PROMOTION</a></p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}
    </div>
@endsection