$(document).ready(function () {
    $('li.choice-lang').on('click', function (e) {
        e.preventDefault();
        var key = $(this).data('key');
        $.ajax({
            url: APP_URL + 'lang',
            type: 'GET',
            data: {lang: key, _token: "pVwIUMVuVSJePtGFlkdAEb5lJhryBcJJxlSRf7xN"},
            success: function (data) {
                var url = window.location.href;
                window.location = url;
            },
            error: function (xhr) {
                console.log(xhr.message);
            }
        });
    });
});

