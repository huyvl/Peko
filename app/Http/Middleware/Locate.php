<?php

namespace App\Http\Middleware;
use Config;
use Closure;
use Session;
use App;

class Locate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('locale')) {
            $locale = Session::get('locale', Config::get('app.locale'));

        } else {
            $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);

            if ($locale != 'en') {
                $locale = 'vn';
            }
        }

        Session::put('locale', $locale);
        App::setLocale($locale);

        return $next($request);
    }
}
