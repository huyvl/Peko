<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::group(['middleware' => 'locate', 'prefix' => Session::get('locale')], function() {
    Route::get('/', 'HomeController@index');
    Route::get('/work', 'HomeController@work');
    Route::get('/shop', 'HomeController@shop');
    Route::get('/partner', 'HomeController@partner');
    Route::get('/signup', 'HomeController@signup');
    Route::get('/guide', 'HomeController@guide');
    Route::get('lang/{lang}', 'HomeController@lang');
});